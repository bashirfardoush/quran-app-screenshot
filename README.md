<h1><b>Quran App Screentshot</b><h1>
This is a premium project. Comming on play store with updated UI and data within 15-Jan-2018.
<br/> 

## Key Technology

* Android
* Java
* Material Design
* View Pager
* Recyclerview
* Retrofit
* Butter Knife
* Room
* Work Manager
* PHP
* MySql

<hr/>
</br></br>


</br>
<h3>Showing juz wise index</h3>
<img src="images/juz_index.jpg" width="360" height="640"/>

<hr/>

<br><br>
<h3>Showing chapter wise index</h3>
<img src="images/chapter_index.jpg"  width="360" height="640"/>

<hr/>

<br></br>
<h3>Juz wise verse groups</h3>
<img src="images/juz_wise_vgs_view.jpg" width="360" height="640" />

<hr/>

<br></br>
<h3>Chapter wise verse groups</h3>
<img src="images/chapter_wise_vgs_view.jpg" width="360" height="640" />

<hr/>

<br></br>
<h3>Details of a single verse group</h3>
<img src="images/vg_detail.jpg" width="360" height="640" />

<hr/>

<br></br>
<h3>Topic expand and shrink</h3>
<img src="images/multiple_.jpg"  width="360" height="640"/>
